const Raspi = require('raspi-io').RaspiIO;
const five = require('johnny-five');
const signale = require('signale');

const board = new five.Board({
    io: new Raspi()
});

board.on('ready', () => {
   signale.success('Placa lista');

   const led = new five.Led('GPIO27');
   const led2 = new five.Led('GPIO23');

   const button = new five.Button({
       pin: 'GPIO22',
       isPullup: true
   });

   button.on("press", function(value){
      led.on();
   });

   
   button.on("up", function(value){
    led.stop();
    led2.stop();
  });

  button.on("release", function(value){
    led.off();
    led2.off();
  });

  button.on("hold", function(value){
    led.strobe();
    led2.strobe();
  });

  signale.watch('waiting for device initialization...')


});

